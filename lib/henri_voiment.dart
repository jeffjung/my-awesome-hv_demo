import 'dart:math';

const List<String> hen = [
  'Hen',
  'Han',
  'En',
  'An',
  'Haaaan',
  'Hein',
  // 'Men',
  // 'Man',
  // 'Ren',
  // 'Ran',
  // 'Nan',
  // 'Nen'
];

const List<String> ri = [
  'ri',
  'riz',
  'ry',
  'rille',
  'rit',
  'ryt',
  'rie',
  'rry',
  'rri',
  'rrhi',
  'rrhy',
  'ricard',
  'riche'
];

const List<String> voi = [
  'Voi',
  'Voie',
  'Voua',
  'Voy',
  'Wuha',
  'Woua',
  'Wouha',
  'Vua',
];

const List<String> ment = [
  'ment',
  'men',
  'man',
  'mant',
  'mhan',
  'lie',
  'mend',
  'mand',
  'mans',
  'mens'
];

Iterable<String> generateVoiment() sync* {
  Random random = Random();

  String pickRandom(List<String> list) => list[random.nextInt(list.length)];

  // We're in a sync* function, so `while (true)` is okay.
  // ignore: literal_only_boolean_expressions
  while (true) {
    yield pickRandom(hen) +
        pickRandom(ri) +
        ' ' +
        pickRandom(voi) +
        pickRandom(ment);
  }
}
