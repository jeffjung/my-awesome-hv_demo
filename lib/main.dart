import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hv_demo/henri_voiment.dart';

void main() {
  LicenseRegistry.addLicense(() async* {
    final license = await rootBundle.loadString('google_fonts/OFL.txt');
    yield LicenseEntryWithLineBreaks(['google_fonts'], license);
  });
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'HV Selektor',
        home: Scaffold(body: Center(child: RandomVoiment())),
        theme: ThemeData(
          primaryColor: Color.fromRGBO(255, 211, 0, 1),
        ));
  }
}

class RandomVoiment extends StatefulWidget {
  @override
  _RandomVoimentState createState() => _RandomVoimentState();
}

class _RandomVoimentState extends State<RandomVoiment> {
  final _hevoiments = <String>[];
  final _saved = <String>{};
  final _biggerFont = TextStyle(fontSize: 18.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HV SELEKTOR',
            style: GoogleFonts.raleway(
                fontSize: 48,
                fontWeight: FontWeight.w900,
                color: Color.fromRGBO(0, 32, 96, 1))),
        actions: [IconButton(icon: Icon(Icons.list), onPressed: _pushSaved)],
      ),
      body: _buildVoiments(),
    );
  }

  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final tiles = _saved.map(
            (String hevoiment) {
              return ListTile(
                title: Text(
                  hevoiment,
                  style: _biggerFont,
                ),
              );
            },
          );
          final divided = ListTile.divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return Scaffold(
            appBar: AppBar(
              title: Text('My favourite Voiment'),
            ),
            body: ListView(children: divided),
          );
        },
      ),
    );
  }

  Widget _buildVoiments() {
    return ListView.builder(
        padding: EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) return Divider();
          final index = i ~/ 2;
          if (index >= _hevoiments.length) {
            _hevoiments.addAll(generateVoiment().take(10));
          }
          return _buildRow(_hevoiments[index]);
        });
  }

  Widget _buildRow(String hevoiment) {
    final alreadySaved = _saved.contains(hevoiment);
    return ListTile(
      title: Text(hevoiment, style: _biggerFont),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: () {
        setState(() {
          if (alreadySaved) {
            _saved.remove(hevoiment);
          } else {
            _saved.add(hevoiment);
          }
        });
      },
    );
  }
}
